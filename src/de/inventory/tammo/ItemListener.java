package de.inventory.tammo;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface ItemListener {

    void interact(final Player player, final Inventory inventory, final ClickType action, final ItemStack stack);

}
