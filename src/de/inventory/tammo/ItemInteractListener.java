package de.inventory.tammo;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;

public interface ItemInteractListener {

    void interact(final Player player, final ItemStack stack, final Action action, final Block clickedBlock);

}
