package de.inventory.tammo;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class InventoryAPI extends JavaPlugin {

    private static InventoryAPI inventoryAPI;

    private InventoryHandler inventoryHandler;

    public void onEnable() {
        inventoryAPI = this;
        this.inventoryHandler = new InventoryHandler();

        Bukkit.getPluginManager().registerEvents(this.inventoryHandler, this);
    }

    public static InventoryAPI getInventoryAPI() {
        return inventoryAPI;
    }

    public final InventoryHandler getInventoryHandler() {
        return this.inventoryHandler;
    }
}
