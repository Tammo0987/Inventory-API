package de.inventory.tammo;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class ItemBuilder {

    private final ItemStack stack;

    private final ItemMeta meta;

    public ItemBuilder(final Material material, final int amount){
        this.stack = new ItemStack(material, amount);
        this.meta = stack.getItemMeta();
    }

    public ItemBuilder(final Material material){
        this.stack = new ItemStack(material, 1);
        this.meta = stack.getItemMeta();
    }

    public ItemBuilder(final Material material, final String displayname){
        this.stack = new ItemStack(material, 1);
        this.meta = stack.getItemMeta();
        this.setDisplayName(displayname);
    }

    public final ItemBuilder setDisplayName(final String displayName){
        this.meta.setDisplayName(displayName);
        return this;
    }

    public final ItemBuilder setDurability(final short durability){
        this.stack.setDurability(durability);
        return this;
    }

    public final ItemBuilder setMaterial(final Material material){
        this.stack.setType(material);
        return this;
    }

    public final ItemBuilder setAmount(final int amount){
        this.stack.setAmount(amount);
        return this;
    }

    public final ItemBuilder addToLore(final String lore){
        final ArrayList<String> loreList = this.meta.getLore() == null ? new ArrayList<String>() : (ArrayList<String>) this.meta.getLore();
        loreList.add(lore);
        this.meta.setLore(loreList);
        return this;
    }

    public final ItemBuilder removeFromLore(final String lore){
        final ArrayList<String> loreList = new ArrayList<>();
        for(final String s : this.meta.getLore()){
            if(!s.equals(lore)){
                loreList.add(s);
            }
        }
        this.meta.setLore(loreList);
        return this;
    }

    public final ItemBuilder setLore(final String... lore){
        this.meta.setLore(Arrays.asList(lore));
        return this;
    }

    public final ItemBuilder removeLore(){
        this.meta.setLore(Arrays.asList());
        return this;
    }

    public final ItemBuilder addEnchantment(final Enchantment enchantment, final int level){
        this.meta.addEnchant(enchantment, level, true);
        return this;
    }

    public final ItemBuilder removeEnchantments(){
        for(final Enchantment enchantment : this.meta.getEnchants().keySet()){
            this.meta.removeEnchant(enchantment);
        }
        return this;
    }

    public final ItemBuilder addItemFlags(final ItemFlag... flag){
        this.meta.addItemFlags(flag);
        return this;
    }

    public final ItemBuilder removeItemFlags(final ItemFlag... flags){
        this.meta.removeItemFlags(flags);
        return this;
    }

    public final ItemStack buildSkull(final String owner){
        final String displayname = this.meta.getDisplayName();
        final SkullMeta meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
        meta.setOwner(owner);
        meta.setDisplayName(displayname);
        this.stack.setItemMeta(meta);
        this.setDurability((short) 3);
        return this.stack;
    }

    public final ItemStack build(){
        this.stack.setItemMeta(this.meta);
        return this.stack;
    }

    public final ItemBuilder addInteractListener(final ItemInteractListener listener){
        this.stack.setItemMeta(this.meta);
        InventoryAPI.getInventoryAPI().getInventoryHandler().registerItemListener(this.stack, listener);
        return this;
    }

}
