package de.inventory.tammo;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class InventoryHandler implements Listener {

    private final HashMap<Inventory, ItemListener> listener = new HashMap<>();

    private final HashMap<ItemStack, ItemInteractListener> itemlistener = new HashMap<>();

    public void registerListener(final Inventory inventory, final ItemListener listener){
        if(!this.listener.containsKey(inventory)){
            this.listener.put(inventory, listener);
        }
    }

    public void registerItemListener(final ItemStack stack, final ItemInteractListener listener){
        if(!this.itemlistener.containsKey(stack)){
            this.itemlistener.put(stack, listener);
        }
    }

    @EventHandler
    public void onClick(final InventoryClickEvent event){
        if(event.getInventory() == null || event.getCurrentItem() == null || event.getWhoClicked() == null){
            return;
        }

        if(event.getCurrentItem() == null){
            return;
        }

        if(this.listener.containsKey(event.getInventory())){
            event.setCancelled(true);
            this.listener.get(event.getInventory()).interact((Player) event.getWhoClicked(), event.getInventory(), event.getClick(), event.getCurrentItem());
        }
    }

    @EventHandler
    public void onInteract(final PlayerInteractEvent event){
        if(event.getPlayer() == null || event.getAction() == null || event.getItem() == null){
            return;
        }

        if(this.itemlistener.containsKey(event.getItem())){
            this.itemlistener.get(event.getItem()).interact(event.getPlayer(), event.getItem(), event.getAction(), event.getClickedBlock());
        }
    }

    @EventHandler
    public void onClose(final InventoryCloseEvent event){
        if(event.getInventory() == null){
            return;
        }
        if(this.listener.containsKey(event.getInventory())){
            this.listener.remove(event.getInventory());
        }
    }

}
