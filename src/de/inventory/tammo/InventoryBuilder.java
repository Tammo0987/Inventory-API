package de.inventory.tammo;


import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class InventoryBuilder {

    private Inventory inventory;

    public InventoryBuilder(final String name, final int size, final InventoryHolder owner){
        this.inventory = Bukkit.createInventory(owner, size, name);
    }

    public InventoryBuilder(final InventoryHolder owner, final InventoryType type){
        this.inventory = Bukkit.createInventory(owner, type);
    }

    public InventoryBuilder(final String name, final int size){
        this.inventory = Bukkit.createInventory(null, size, name);
    }

    public InventoryBuilder addListener(final ItemListener listener){
        InventoryAPI.getInventoryAPI().getInventoryHandler().registerListener(this.inventory, listener);
        return this;
    }

    public void refresh(){
        for(final HumanEntity holder : this.inventory.getViewers()){
            holder.closeInventory();
            holder.openInventory(this.inventory);
        }
    }

    public InventoryBuilder addItem(final ItemStack stack){
        this.inventory.addItem(stack);
        return this;
    }

    public InventoryBuilder setItem(final int slot, final ItemStack stack){
        this.inventory.setItem(slot, stack);
        return this;
    }

    public InventoryBuilder fill(final ItemStack stack){
        for(int i = 0; i < this.inventory.getSize(); i++){
            if(this.inventory.getItem(i) == null){
                this.inventory.setItem(i, stack);
            }
        }
        return this;
    }

    public Inventory build(){
        return this.inventory;
    }

    public InventoryBuilder show(final HumanEntity... viewers){
        this.refresh();
        for(final HumanEntity viewer : viewers){
            viewer.openInventory(this.build());
        }
        return this;
    }
}
